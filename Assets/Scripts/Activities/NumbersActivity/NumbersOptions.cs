﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumbersOptions : ActivityOptions
{
    [Header("Numbers options")]
    public List<string> numbersAvailable;

    public override void Init(Activity _activity)
    {
        possibleAnswers = (uint)Mathf.Min(possibleAnswers, numbersAvailable.Count); // In case numbersAvailable is smaller than possibleAnswers
        base.Init(_activity);
    }

    protected override void CreateAnswerButtons()
    {
        // Fill a list with all numeric values available
        List<uint> numbers = new List<uint>();
        for (uint i = 0; i < numbersAvailable.Count; ++i)
        {
            numbers.Add(i);
        }

        List<GameObject> answers = new List<GameObject>();
        answers.Add(CreateButton(correctAnswer));
        numbers.RemoveAt((int)correctAnswer);

        for (int i = 0; i < possibleAnswers-1; ++i)
        {
            // Pick a random number from the list
            int index = Random.Range(0, numbers.Count);
            answers.Add(CreateButton(numbers[index]));
            numbers.RemoveAt(index);
        }

        // Re-assign randomly to parent to shuffle the buttons
        for (int i = 0; i < possibleAnswers; ++i) // possibleAnswers == answers.Count
        {
            int index = Random.Range(0, answers.Count);
            answers[index].transform.SetParent(transform);
            answers[index].transform.SetParent(optionsArea.transform);
            answers.RemoveAt(index);
        }
    }
    
    GameObject CreateButton(uint answer)
    {
        ActivityAnswerButton instance = Instantiate(answerButton, optionsArea.transform).GetComponent<ActivityAnswerButton>();

        instance.Init(activity.activityManager);
        instance.text.text = answer.ToString();

        if (answer == correctAnswer)
        {
            correctButton = instance;
        }

        options.Add(instance);

        return instance.gameObject;
    }

    public string GetCorrectAnswer()
    {
        correctAnswer = (uint)Random.Range(0, numbersAvailable.Count);
        return numbersAvailable[(int)correctAnswer];
    }
}
