﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumbersStatement : ActivityStatement
{
    protected override ActivityStatementButton CreateStatement()
    {
        ActivityStatementButton instance = Instantiate(statementObject, statementArea.transform).GetComponent<ActivityStatementButton>();
        instance.text.text = ((NumbersOptions)activity.options).GetCorrectAnswer();

        return instance;
    }
}
