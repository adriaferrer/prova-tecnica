﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityEvents : MonoBehaviour
{
    public static ActivityEvents current;

    private void Awake()
    {
        current = this;
    }

    public event Action<ActivityAnswerButton> onButtonClicked;
    public void ButtonClicked(ActivityAnswerButton button)
    {
        if (onButtonClicked != null)
        {
            onButtonClicked(button);
        }
    }

    public event Action onEnableInteraction;
    public IEnumerator EnableInteraction(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        
        if (onEnableInteraction != null)
        {
            onEnableInteraction();
        }
    }
    
    public event Action onDisableInteraction;
    public void DisableInteraction()
    {
        if (onDisableInteraction != null)
        {
            onDisableInteraction();
        }
    }
}
