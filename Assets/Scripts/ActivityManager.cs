﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityManager : MonoBehaviour
{
    public Color correctAnswerColor;
    public Color incorrectAnswerColor;

    public ActivityScore score;

    public Activity[] activities;
    int currentActivity;

    // Start is called before the first frame update
    void Start()
    {
        currentActivity = 0;

        foreach (var activity in activities)
        {
            activity.Init(this);
        }
        
        if (activities.Length > 0)
            activities[0].StartStatement();
    }

    public void ActivityFinished(bool success)
    {
        score.NewScore(success);

        currentActivity++;
        if (currentActivity == activities.Length) // Restart after reached last activity
            currentActivity = 0;

        activities[currentActivity].StartStatement();
    }
}
