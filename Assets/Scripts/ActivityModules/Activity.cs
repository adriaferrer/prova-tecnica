﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Activity : MonoBehaviour
{
    public ActivityStatement statement;
    public ActivityOptions options;

    [HideInInspector]
    public ActivityManager activityManager;

    public void Init(ActivityManager manager)
    {
        activityManager = manager;

        statement.Init(this);
        options.Init(this);
    }

    public void StartStatement()
    {
        StartCoroutine(statement.RunStatement());
    }

    public void StartOptions()
    {
        options.RunOptions();
    }

    public void EndActivity(bool success)
    {
        // Notify manager that the activity has finished
        activityManager.ActivityFinished(success);
    }
}
