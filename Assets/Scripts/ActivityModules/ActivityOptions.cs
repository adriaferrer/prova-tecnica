﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivityOptions : MonoBehaviour
{
    protected Activity activity;
    
    [Range(2, 10)]
    public uint possibleAnswers = 3;
    
    public GameObject answerButton;
    public GameObject optionsArea;
    
    protected List<ActivityAnswerButton> options;
    protected uint correctAnswer;
    protected ActivityAnswerButton correctButton;

    private uint triesLeft;

    public virtual void Init(Activity _activity)
    {
        activity = _activity;

        options = new List<ActivityAnswerButton>();
        triesLeft = possibleAnswers;
        
        //Subscribe event listeners
        ActivityEvents.current.onButtonClicked += AnswerGiven;
    }
    
    public void RunOptions()
    {
        //Reset tries
        triesLeft = possibleAnswers;
        
        // Create buttons
        CreateAnswerButtons();

        // Disable interaction
        ActivityEvents.current.DisableInteraction();

        // Play animationIn on all buttons
        float duration = 0.5f;
        foreach (ActivityAnswerButton button in options)
        {
            if (button.animated)
            {
                duration = button.animated.AnimateIn();
            }
        }

        // Activa botons interactuables
        StartCoroutine(ActivityEvents.current.EnableInteraction(duration));
    }

    public IEnumerator FinishOptions(bool success)
    {
        // Play animationOut on all buttons
        float duration = 0.5f;
        foreach (ActivityAnswerButton button in options)
        {
            if (button.animated && button.animated.enabled)
            {
                duration = button.animated.AnimateOut();
            }
        }

        yield return new WaitForSeconds(duration);

        foreach (ActivityAnswerButton button in options)
        {
            Destroy(button.gameObject);
        }
        options.Clear();
        
        activity.EndActivity(success);
    }

    private void AnswerGiven(ActivityAnswerButton button)
    {
        ActivityEvents.current.DisableInteraction();
        
        if (button.GetInstanceID() == correctButton.GetInstanceID())
        {
            button.TintCorrect();
            StartCoroutine(FinishOptions(true));
        }
        else
        {
            button.TintIncorrect();
            triesLeft--;

            if (triesLeft > 1)
            {
                if (button.animated)
                {
                    float duration = button.animated.AnimateOut();
                    StartCoroutine(ActivityEvents.current.EnableInteraction(duration));
                }
            }
            else
            {
                correctButton.TintCorrect();
                StartCoroutine(FinishOptions(false));
            }
        }
    }
    
    private void OnDestroy()
    {
        ActivityEvents.current.onButtonClicked -= AnswerGiven;
    }
    
    protected abstract void CreateAnswerButtons();
}
