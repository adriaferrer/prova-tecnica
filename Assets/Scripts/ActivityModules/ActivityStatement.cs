﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivityStatement : MonoBehaviour
{
    protected Activity activity;
    
    public float statementDuration;
    
    public GameObject statementObject;
    public GameObject statementArea;

    public void Init(Activity _activity)
    {
        activity = _activity;
    }
    
    public IEnumerator RunStatement()
    {
        // Create statement object
        ActivityStatementButton statement = CreateStatement();

        // Play animationIn
        float duration = 0.5f;
        if (statement.animated)
        {
            duration = statement.animated.AnimateIn();
        }
        
        yield return new WaitForSeconds(statementDuration+duration);

        // Play animationOut
        if (statement.animated)
        {
            duration = statement.animated.AnimateOut();
        }

        // Wait for the animation and destroy object
        yield return new WaitForSeconds(duration);
        Destroy(statement.gameObject);

        // Start Answer part
        activity.StartOptions();
    }
    
    protected abstract ActivityStatementButton CreateStatement();
}
