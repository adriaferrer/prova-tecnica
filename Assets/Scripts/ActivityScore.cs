﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityScore : MonoBehaviour
{
    public Text successesText;
    public Text failuresText;
    
    int successes = 0;
    int failures = 0;

    public void NewScore(bool success)
    {
        if (success)
        {
            successes++;
            successesText.text = successes.ToString();
        }
        else
        {
            failures++;
            failuresText.text = failures.ToString();
        }
    }
}
