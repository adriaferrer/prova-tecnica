﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class ActivityAnimatedElement : MonoBehaviour
{
    public Animation animation;

    public AnimationClip animationIn;
    public AnimationClip animationOut;

    [HideInInspector]
    public bool enabled = true;

    public float AnimateIn()
    {
        animation.Play(animationIn.name);

        return animationIn.length;
    }

    public float AnimateOut()
    {
        enabled = false;
        animation.Play(animationOut.name);

        return animationOut.length;
    }

    public void OnFinished()
    {
        enabled = false;
    }
}
