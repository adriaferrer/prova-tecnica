﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ActivityAnswerButton : MonoBehaviour
{
    public Image image;
    public Button button;
    public Text text;
    public ActivityAnimatedElement animated;

    ActivityManager activityManager;

    public void Init(ActivityManager manager)
    {
        activityManager = manager;
        
        //Subscribe event listeners
        ActivityEvents.current.onEnableInteraction += EnableInteraction;
        ActivityEvents.current.onDisableInteraction += DisableInteraction;
    }
    
    public void OnClick()
    {
        ActivityEvents.current.ButtonClicked(this);
    }

    public void TintCorrect()
    {
        image.color = activityManager.correctAnswerColor;
    }

    public void TintIncorrect()
    {
        image.color = activityManager.incorrectAnswerColor;
    }

    private void EnableInteraction()
    {
        button.interactable = true;
    }
    
    private void DisableInteraction()
    {
        button.interactable = false;
    }

    private void OnDestroy()
    {
        ActivityEvents.current.onEnableInteraction -= EnableInteraction;
        ActivityEvents.current.onDisableInteraction -= DisableInteraction;
    }
}
